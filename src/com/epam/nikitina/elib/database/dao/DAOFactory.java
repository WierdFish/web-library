package com.epam.nikitina.elib.database.dao;

import com.epam.nikitina.elib.database.dao.interfaces.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class DAOFactory {
    private final static Logger logger = LogManager.getLogger(DAOFactory.class);

    private static BooksDAO booksDAO = null;
    private static JournalDAO journalDAO = null;
    private static AdminDAO adminDAO = null;
    private static ManageDAO manageDAO = null;
    private static ReaderDAO readerDAO = null;
    private static DAOFactory instance = null;

    private DAOFactory() {

    }

    public static DAOFactory getInstance() {
        if (instance == null) {
            instance = new DAOFactory();
            logger.debug("DAOFactory init");
        }
        return instance;
    }

    public BooksDAO getBooksDAO() {
        if (booksDAO == null) {
            booksDAO = new BooksDAOImpl();
            logger.debug("BooksDAO init");

        }
        return booksDAO;
    }

    public static JournalDAO getJournalDAO() {
        if (journalDAO == null) {
            journalDAO = new JournalDAOImpl();
            logger.debug("JournalDAO init");

        }
        return journalDAO;
    }

    public static AdminDAO getAdminDAO() {
        if (adminDAO == null) {
            adminDAO = new AdminDAOImpl();
            logger.debug("AdminDAO init");

        }
        return adminDAO;
    }

    public static ManageDAO getManageDAO() {
        if (manageDAO == null) {
            manageDAO = new ManageDAOImpl();
            logger.debug("ManageDAO init");

        }
        return manageDAO;
    }

    public static ReaderDAO getReaderDAO() {
        if (readerDAO == null) {
            readerDAO = new ReaderDAOImpl();
            logger.debug("ReaderDAO init");

        }
        return readerDAO;
    }
}
