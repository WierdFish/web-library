package com.epam.nikitina.elib.database.dao;

import com.epam.nikitina.elib.database.dao.interfaces.BooksDAO;
import com.epam.nikitina.elib.database.entities.BooksEntity;
import com.epam.nikitina.elib.database.util.HibernateSessionFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class BooksDAOImpl implements BooksDAO {
    private final Logger logger = LogManager.getLogger(BooksDAOImpl.class);

    @Override
    public List<BooksEntity> getPage(int offset, int count) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Query query = session.createQuery("FROM BooksEntity ORDER BY authorLastName, authorFirstName");
        query.setFirstResult(offset);
        query.setMaxResults(count);
        List<BooksEntity> books = query.list();
        session.close();
        logger.debug("Get books page from " + offset + " to " + count);
        return books;
    }

    @Override
    public void create(BooksEntity book) {
        Transaction tx = null;

        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            tx = session.beginTransaction();
            session.save(book);
            tx.commit();
            logger.debug("Book added: " + book.getAuthorFirstName() + " " + book.getAuthorLastName() + " title: "
                    + book.getTitle() + " year: " + book.getYear() + " amount:" + book.getAmount());
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("Can't add book: ", e);
        }
    }

    @Override
    public void remove(int code) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            BooksEntity book = session.load(BooksEntity.class, code);
            session.delete(book);
            tx.commit();
            logger.debug("Book id:" + code + " deleted");
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("Can't remove book: ", e);
        } finally {
            session.close();
        }
    }

    @Override
    public void update(BooksEntity book) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(book);
            tx.commit();
            logger.debug("Book id:" + book.getCode() + " updated: " + book.getAuthorFirstName()
                    + " " + book.getAuthorLastName() + " title: " + book.getTitle() + " year: "
                    + book.getYear() + " amount:" + book.getAmount());
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("Can't update book: ", e);
        } finally {
            session.close();
        }
    }

    @Override
    public BooksEntity getBook(int id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        BooksEntity book = session.get(BooksEntity.class, id);
        session.close();
        logger.debug("Get book id:" + id);
        return book;
    }

    @Override
    public Integer getCountAll() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Query query = session.createQuery("SELECT COUNT(code) from BooksEntity");
        Integer count = ((Long) query.list().get(0)).intValue();
        session.close();
        logger.debug("Table books row count: " + count);
        return count;
    }
}
