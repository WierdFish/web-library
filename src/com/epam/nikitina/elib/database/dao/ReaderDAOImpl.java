package com.epam.nikitina.elib.database.dao;

import com.epam.nikitina.elib.beans.UserBean;
import com.epam.nikitina.elib.database.dao.interfaces.ReaderDAO;
import com.epam.nikitina.elib.database.entities.ReadersEntity;
import com.epam.nikitina.elib.database.util.HibernateSessionFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ReaderDAOImpl implements ReaderDAO {
    private final Logger logger = LogManager.getLogger(ReaderDAOImpl.class);

    @Override
    public UserBean getUser(String login) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        ReadersEntity reader = null;
        try {
            Transaction tx = session.beginTransaction();
            reader = session.get(ReadersEntity.class, login);
            tx.commit();
            logger.debug("Got user info from database: "+login);
        } catch (HibernateException e) {
            logger.error("Can't get user info: ", e);
        } finally {
            session.close();
            return new UserBean(reader);
        }
    }
}
