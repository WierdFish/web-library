package com.epam.nikitina.elib.database.dao;

import com.epam.nikitina.elib.database.dao.interfaces.JournalDAO;
import com.epam.nikitina.elib.database.entities.BooksEntity;
import com.epam.nikitina.elib.database.entities.JournalEntity;
import com.epam.nikitina.elib.database.entities.ReadersEntity;
import com.epam.nikitina.elib.database.util.HibernateSessionFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Collections;
import java.util.List;

public class JournalDAOImpl implements JournalDAO {
    private final Logger logger = LogManager.getLogger(JournalDAOImpl.class);

    @Override
    public void create(JournalEntity note) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(note);
            tx.commit();
            logger.debug("Created note id:" + note.getNoteId());
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("Can't create journal note: ", e);
        } finally {
            session.close();
        }
    }

    @Override
    public List<JournalEntity> getPage(int offset, int count) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Query query = session.createQuery("FROM JournalEntity ORDER BY  accepted,  returnDate, reserveDate DESC");
        query.setFirstResult(offset);
        query.setMaxResults(count);
        List<JournalEntity> notes = query.list();
        session.close();
        logger.debug("Get notes from " + offset + " to" + (offset + notes.size()));
        return notes;
    }

    @Override
    public List<JournalEntity> getByUser(String login, int offset, int count) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        ReadersEntity reader = session.load(ReadersEntity.class, login);
        List<JournalEntity> notes = reader.getReaderHistory();
        session.close();
        Collections.reverse(notes);
        int max;
        if (offset + count > notes.size()) {
            max = notes.size();
        } else {
            max = count + offset;
        }
        logger.debug("Get user " + login + " notes from " + offset + "to " + max);
        return notes.subList(offset, max);
    }

    @Override
    public void updateHold(int id, String hold) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx;
        try {
            tx = session.beginTransaction();
            JournalEntity je = session.load(JournalEntity.class, id);
            je.setHold(hold);
            tx.commit();
            logger.debug("Note id:" + id + " hold updated: " + hold);
        } catch (Exception e) {
            logger.error("Can't update journal note: ", e);
        } finally {
            session.close();
        }
    }

    @Override
    public Integer getUserCount(String login) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        ReadersEntity reader = session.load(ReadersEntity.class, login);
        List<JournalEntity> notes = reader.getReaderHistory();
        session.close();
        logger.debug("User " + login + " notes count: " + notes.size());
        return notes.size();
    }

    @Override
    public Integer getAllCount() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Query query = session.createQuery("SELECT COUNT(noteId) FROM JournalEntity");
        Integer num = ((Long) query.list().get(0)).intValue();
        session.close();
        logger.debug("Table Journal row count: " + num);
        return num;
    }
}
