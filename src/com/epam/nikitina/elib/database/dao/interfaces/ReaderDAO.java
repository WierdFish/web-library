package com.epam.nikitina.elib.database.dao.interfaces;

import com.epam.nikitina.elib.beans.UserBean;

public interface ReaderDAO {
    UserBean getUser(String login);
}
