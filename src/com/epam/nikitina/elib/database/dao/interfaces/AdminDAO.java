package com.epam.nikitina.elib.database.dao.interfaces;

import java.util.Date;

public interface AdminDAO {
   boolean isLoginFree(String login);
   boolean registerUser(String login, String password, String first, String last, Date birth);
   boolean validate (String login, String password);
}
