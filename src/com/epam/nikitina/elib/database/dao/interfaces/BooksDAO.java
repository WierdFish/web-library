package com.epam.nikitina.elib.database.dao.interfaces;


import com.epam.nikitina.elib.database.entities.BooksEntity;

import java.util.List;

public interface BooksDAO {
    List<BooksEntity> getPage(int offset, int count);

    void create(BooksEntity book);

    void remove(int code);

    void update(BooksEntity book);

    BooksEntity getBook(int id);

    Integer getCountAll();

}
