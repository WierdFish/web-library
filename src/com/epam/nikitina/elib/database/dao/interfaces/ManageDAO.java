package com.epam.nikitina.elib.database.dao.interfaces;

public interface ManageDAO {
    void confirmReturn(int noteId);

    void rejectRequest(int noteId);

    void requestBook(String login, int code, String hold);

    void acceptRequest(int noteId);

}
