package com.epam.nikitina.elib.database.dao.interfaces;

import com.epam.nikitina.elib.database.entities.JournalEntity;

import java.util.List;

public interface JournalDAO {
    void create(JournalEntity note);

    List<JournalEntity> getPage(int offset, int count);

    List<JournalEntity> getByUser(String login, int offset, int count);

    Integer getUserCount(String login);

    Integer getAllCount();

    void updateHold(int id, String hold);

}
