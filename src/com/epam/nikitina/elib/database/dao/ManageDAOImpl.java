package com.epam.nikitina.elib.database.dao;

import com.epam.nikitina.elib.database.dao.interfaces.ManageDAO;
import com.epam.nikitina.elib.database.entities.BooksEntity;
import com.epam.nikitina.elib.database.entities.JournalEntity;
import com.epam.nikitina.elib.database.entities.ReadersEntity;
import com.epam.nikitina.elib.database.util.HibernateSessionFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ManageDAOImpl implements ManageDAO {
    private final Logger logger = LogManager.getLogger(ManageDAOImpl.class);

    @Override
    public void confirmReturn(int noteId) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            JournalEntity note = session.load(JournalEntity.class, noteId);
            Date date = new Date();
            note.setReturnDate(new Timestamp(date.getTime()));
            BooksEntity book = note.getBooksEntity();
            book.setAmount(book.getAmount() + 1);
            tx.commit();
            logger.debug("Note id: " + noteId + " return date: " + note.getReturnDate());
            logger.debug("Book id:" + book.getCode() + " new amount: " + book.getAmount());
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("Can't confirm return id:" + noteId, e);
        } finally {
            session.close();
        }
    }

    @Override
    public void rejectRequest(int noteId) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            JournalEntity note = session.load(JournalEntity.class, noteId);

            ReadersEntity reader = note.getReadersEntity();
            BooksEntity book = note.getBooksEntity();
            for (Iterator iter = reader.getReaderHistory().iterator(); iter.hasNext(); ) {
                JournalEntity cur = (JournalEntity) iter.next();
                if (cur.getNoteId() == noteId) {
                    iter.remove();
                    break;
                }
            }
            for (Iterator iter = book.getBookHistory().iterator(); iter.hasNext(); ) {
                JournalEntity cur = (JournalEntity) iter.next();
                if (cur.getNoteId() == noteId) {
                    iter.remove();
                    break;
                }
            }

            book.setAmount(book.getAmount() + 1);
            session.update(book);
            session.update(reader);
            session.delete(note);

            logger.debug("Note with id: " + noteId + " deleted. Book id:" + book.getCode() + " new amount: " + book.getAmount());
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("Can't delete journal note: id:" + noteId, e);
        } finally {
            session.close();
        }
    }

    @Override
    public void requestBook(String login, int code, String hold) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            ReadersEntity reader = session.load(ReadersEntity.class, login);
            BooksEntity book = session.load(BooksEntity.class, code);
            JournalEntity note = new JournalEntity(hold, reader, book);
            session.save(note);
            book.setAmount(book.getAmount() - 1);
            tx.commit();
            logger.debug("Note id:" + note.getNoteId() + "created. Book id:" + book.getCode() + " new amount: " + book.getAmount());
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("Can't create journal note: ", e);
        } finally {
            session.close();
        }
    }

    @Override
    public void acceptRequest(int noteId) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            JournalEntity note = session.load(JournalEntity.class, noteId);
            note.setAccepted(true);
            tx.commit();
            logger.debug("Request id:" + noteId + " accepted");
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("Can't accept request id:" + noteId, e);
        } finally {
            session.close();
        }
    }
}
