package com.epam.nikitina.elib.database.dao;

import com.epam.nikitina.elib.database.dao.interfaces.AdminDAO;
import com.epam.nikitina.elib.database.entities.LoginDataEntity;
import com.epam.nikitina.elib.database.entities.ReadersEntity;
import com.epam.nikitina.elib.database.util.HibernateSessionFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Date;
import java.util.List;

public class AdminDAOImpl implements AdminDAO {
    private final Logger logger = LogManager.getLogger(AdminDAOImpl.class);

    @Override
    public boolean validate(String login, String password) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        LoginDataEntity userData = session.get(LoginDataEntity.class, login);
        if (userData != null && userData.getPassword().equals(password)) {
            session.close();
            return true;
        }
        session.close();
        return false;
    }

    @Override
    public boolean isLoginFree(String login) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        List user = session.createQuery("FROM LoginDataEntity WHERE login='" + login + "'").list();
        session.close();
        logger.debug("Login " + login + " is free: " + user.isEmpty());
        return (user.isEmpty());
    }

    @Override
    public boolean registerUser(String login, String password, String first, String last, Date birth) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            LoginDataEntity loginData = new LoginDataEntity(login, password);
            ReadersEntity reader = new ReadersEntity(login, first, last, birth, new Date());
            reader.setLoginDataEntity(loginData);
            session.save(loginData);
            session.save(reader);
            tx.commit();
            logger.debug("Added user " + login + ": " + first + " " + last + " birth:" + birth);
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("Can't register user: ", e);
            return false;
        } finally {
            session.close();
        }
        return true;
    }
}
