package com.epam.nikitina.elib.database.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Entity
@Table(name = "journal", schema = "db_library")
public class JournalEntity implements Serializable {
    private int noteId;
    private Timestamp reserveDate;
    private Timestamp returnDate;
    private String hold;
    private ReadersEntity readersEntity;
    private BooksEntity booksEntity;
    private boolean accepted;

    @Column(name ="accepted")
    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public JournalEntity(){
        accepted = false;
    }
    public JournalEntity(String hold, ReadersEntity readersEntity, BooksEntity booksEntity){
        Date date = new Date();
        long time = date.getTime();
        this.reserveDate = new Timestamp(time);
        this.readersEntity = readersEntity;
        this.booksEntity = booksEntity;
        this.hold=hold;
        accepted = false;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name="login")
    public ReadersEntity getReadersEntity() {
        return readersEntity;
    }

    public void setReadersEntity(ReadersEntity readersEntity) {
        this.readersEntity = readersEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER , cascade = CascadeType.PERSIST)
    @JoinColumn(name="code")
    public BooksEntity getBooksEntity() {
        return booksEntity;
    }

    public void setBooksEntity(BooksEntity booksEntity) {
        this.booksEntity = booksEntity;
    }


    @Id
    @Column(name = "note_id", nullable = false)
    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    @Basic
    @Column(name = "reservedate", nullable = false)
    public Timestamp getReserveDate() {
        return reserveDate;
    }

    public void setReserveDate(Timestamp reservedate) {
        this.reserveDate = reservedate;
    }

    @Basic
    @Column(name = "returndate", nullable = true)
    public Timestamp getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Timestamp returndate) {
        this.returnDate = returndate;
    }

    @Basic
    @Column(name = "hold", nullable = false, length = 45)
    public String getHold() {
        return hold;
    }

    public void setHold(String hold) {
        this.hold = hold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JournalEntity that = (JournalEntity) o;

        if (noteId != that.noteId) return false;
       // if (code != that.code) return false;
        //if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (reserveDate != null ? !reserveDate.equals(that.reserveDate) : that.reserveDate != null) return false;
        if (returnDate != null ? !returnDate.equals(that.returnDate) : that.returnDate != null) return false;
        if (hold != null ? !hold.equals(that.hold) : that.hold != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = noteId;
      //  result = 31 * result + (login != null ? login.hashCode() : 0);
      //  result = 31 * result + code;
        result = 31 * result + (reserveDate != null ? reserveDate.hashCode() : 0);
        result = 31 * result + (returnDate != null ? returnDate.hashCode() : 0);
        result = 31 * result + (hold != null ? hold.hashCode() : 0);
        return result;
    }
}
