package com.epam.nikitina.elib.database.entities;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "logindata", schema = "db_library")
public class LoginDataEntity implements Serializable {
    private String login;
    private String password;

    @Column(name = "admin")
    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    private boolean admin;

    public LoginDataEntity() {

    }

    public LoginDataEntity(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public void setReadersEntity(ReadersEntity readersEntity) {
        this.readersEntity = readersEntity;
    }

    private ReadersEntity readersEntity;

    @Id
    @Column(name = "login", nullable = false, length = 45)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "login", referencedColumnName = "login")
    public ReadersEntity getReadersEntity() {
        return readersEntity;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 45)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoginDataEntity that = (LoginDataEntity) o;

        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
