package com.epam.nikitina.elib.database.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "books", schema = "db_library")
public class BooksEntity implements Serializable {
    private int code;
    private String authorFirstName;
    private String authorLastName;
    private String title;
    private int year;
    private int amount;
    private List<JournalEntity> bookHistory;

    public BooksEntity() {

    }

    public BooksEntity(String authorFirstName, String authorLastName, String title, int year, int amount) {
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
        this.title = title;
        this.year = year;
        this.amount = amount;
    }

    @OneToMany(fetch = FetchType.EAGER,
            mappedBy = "booksEntity",
            cascade = CascadeType.ALL)
    public List<JournalEntity> getBookHistory() {
        return bookHistory;
    }

    public void setBookHistory(List<JournalEntity> bookHistory) {
        this.bookHistory = bookHistory;
    }

    @Id
    @Column(name = "code", nullable = false)
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Basic
    @Column(name = "authorfirstname", nullable = false, length = 45)
    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorfirstname) {
        this.authorFirstName = authorfirstname;
    }

    @Basic
    @Column(name = "authorlastname", nullable = false, length = 45)
    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorlastname) {
        this.authorLastName = authorlastname;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 45)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "year", nullable = false, length = 4)
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Basic
    @Column(name = "amount", nullable = false)
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BooksEntity that = (BooksEntity) o;

        if (code != that.code) return false;
        if (amount != that.amount) return false;
        if (authorFirstName != null ? !authorFirstName.equals(that.authorFirstName) : that.authorFirstName != null)
            return false;
        if (authorLastName != null ? !authorLastName.equals(that.authorLastName) : that.authorLastName != null)
            return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        //  if (year != null ? !year.equals(that.year) : that.year != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code;
        result = 31 * result + (authorFirstName != null ? authorFirstName.hashCode() : 0);
        result = 31 * result + (authorLastName != null ? authorLastName.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        //result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + amount;
        return result;
    }
}
