package com.epam.nikitina.elib.database.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "readers", schema = "db_library")
public class ReadersEntity implements Serializable {
    private String login;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private Date registryDate;
    private List<JournalEntity> readerHistory;

    public ReadersEntity() {

    }

    public ReadersEntity(String login, String firstName, String lastName, Date birthDate, Date registryDate) {
        readerHistory = new ArrayList<>();
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.registryDate = registryDate;
    }

    @OneToMany(fetch = FetchType.EAGER,
            mappedBy = "readersEntity",
            cascade = CascadeType.ALL)
    public List<JournalEntity> getReaderHistory() {
        return readerHistory;
    }

    public void setReaderHistory(List<JournalEntity> readerHistory) {
        this.readerHistory = readerHistory;
    }

    @Id
    @Column(name = "login", nullable = false, length = 45)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "firstname", nullable = false, length = 45)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    @Basic
    @Column(name = "lastname", nullable = false, length = 45)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastname) {
        this.lastName = lastname;
    }

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "birthdate", nullable = false)
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthdate) {
        this.birthDate = birthdate;
    }

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "registrydate", nullable = false)
    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registrydate) {
        this.registryDate = registrydate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReadersEntity that = (ReadersEntity) o;

        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (birthDate != null ? !birthDate.equals(that.birthDate) : that.birthDate != null) return false;
        if (registryDate != null ? !registryDate.equals(that.registryDate) : that.registryDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (registryDate != null ? registryDate.hashCode() : 0);
        return result;
    }

    private LoginDataEntity loginDataEntity;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "readersEntity", optional = false, cascade = CascadeType.ALL)
    public LoginDataEntity getLoginDataEntity() {
        return loginDataEntity;
    }

    public void setLoginDataEntity(LoginDataEntity loginDataEntity) {
        this.loginDataEntity = loginDataEntity;
    }
}
