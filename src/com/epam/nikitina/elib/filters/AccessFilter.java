package com.epam.nikitina.elib.filters;


import com.epam.nikitina.elib.beans.UserBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AccessFilter implements Filter {
    private final Logger logger = LogManager.getLogger(AccessFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        String url = req.getRequestURI();
        HttpSession session = req.getSession();
        UserBean data = (UserBean) session.getAttribute("userData");

        if (url.contains("/authorization") || url.contains("/registration")) {
            if (data != null && servletRequest.getParameter("log") == null) {
                logger.info(data.getLogin() + " Access to auth/reg page denied");
                resp.sendRedirect("/app/index");
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } else if (url.contains("/personal")) {
            if (data == null) {
                logger.info("Access to personal page denied");
                resp.sendRedirect("/app/authorization");
            } else if (data.isAdmin()) {
                logger.info(data.getLogin() + " Access to personal page denied. Redirect to control panel");
                resp.sendRedirect("/admin/panel");
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } else if (url.contains("/admin")) {
            if (data == null) {
                logger.info("Guest: Access to admin page denied");
                resp.sendRedirect("/app/authorization");
            } else if (!data.isAdmin()) {
                logger.info(data.getLogin() + " Access to admin page denied. Redirect to cabinet");
                resp.sendRedirect("/personal/cabinet");
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        }
        return;
    }

    @Override
    public void destroy() {

    }
}
