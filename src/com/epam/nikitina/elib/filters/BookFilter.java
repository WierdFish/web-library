package com.epam.nikitina.elib.filters;

import com.epam.nikitina.elib.beans.UserBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class BookFilter implements Filter {
    private final Logger logger = LogManager.getLogger(BookFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        String action = req.getParameter("action");
        HttpSession session = req.getSession();
        String user = ((UserBean) session.getAttribute("userData")).getLogin();
        if (action == null) {
            resp.sendRedirect("/app/admin/book?action=add");
            return;
        } else if (!action.equals("add")) {
            String id = req.getParameter("id");
            if (id == null) {
                logger.info(user + " Incorrect update book request. Changed to add request");
                resp.sendRedirect("/app/admin/book?action=add");
                return;
            }
        }
        filterChain.doFilter(req, resp);
    }

    @Override
    public void destroy() {

    }
}
