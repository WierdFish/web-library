package com.epam.nikitina.elib.api;

import com.epam.nikitina.elib.beans.BooksBean;
import com.epam.nikitina.elib.database.dao.DAOFactory;
import com.epam.nikitina.elib.database.entities.BooksEntity;

import java.util.ArrayList;
import java.util.List;

public class BookList {
    private DAOFactory factory;

    public BookList() {
        factory = DAOFactory.getInstance();
    }



    public ArrayList<BooksBean> getPage(int offset, int count) {
        List<BooksEntity> list = factory.getBooksDAO().getPage(offset, count);
        return toBeanList(list);
    }

    private ArrayList<BooksBean> toBeanList(List<BooksEntity> entities) {
        ArrayList<BooksBean> beans = new ArrayList<>();
        for (BooksEntity book : entities) {
            beans.add(new BooksBean(book));
        }
        return beans;
    }

    public BooksBean getBook(int id) {
        BooksEntity book = factory.getBooksDAO().getBook(id);
        return new BooksBean(book);
    }

    public Integer getBooksCount() {
        return factory.getBooksDAO().getCountAll();
    }
}
