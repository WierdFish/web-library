package com.epam.nikitina.elib.api;

import com.epam.nikitina.elib.beans.JournalBean;
import com.epam.nikitina.elib.database.dao.DAOFactory;
import com.epam.nikitina.elib.database.entities.BooksEntity;
import com.epam.nikitina.elib.database.entities.JournalEntity;

import java.util.ArrayList;
import java.util.List;

public class Librarian {
    private DAOFactory factory;

    public Librarian() {
        factory = DAOFactory.getInstance();
    }

    public void addBook(String firstName, String lastName, String title, int year, int amount) {
        factory.getBooksDAO().create(new BooksEntity(firstName, lastName, title, year, amount));
    }

    public void removeBook(int code) {
        factory.getBooksDAO().remove(code);
    }

    public void updateBook(int id, String firstName, String lastName, String title, int year, int amount) {
        BooksEntity book = new BooksEntity(firstName, lastName, title, year, amount);
        book.setCode(id);
        factory.getBooksDAO().update(book);
    }

    public void rejectRequest(int id) {
        factory.getManageDAO().rejectRequest(id);
    }

    public void confirmReturn(int id) {
        factory.getManageDAO().confirmReturn(id);
    }

    public void acceptRequest(int id) {
        factory.getManageDAO().acceptRequest(id);
    }

    public ArrayList<JournalBean> getNotesPage(int offset, int count) {
        List<JournalEntity> list = factory.getJournalDAO().getPage(offset, count);
        return toBeanList(list);
    }

    private ArrayList<JournalBean> toBeanList(List<JournalEntity> entities) {
        ArrayList<JournalBean> beans = new ArrayList<>();
        for (JournalEntity note : entities) {
            beans.add(new JournalBean(note));
        }
        return beans;
    }

    public Integer getNotesCount() {
        return factory.getJournalDAO().getAllCount();
    }
}
