package com.epam.nikitina.elib.api;

import com.epam.nikitina.elib.beans.JournalBean;
import com.epam.nikitina.elib.database.dao.DAOFactory;
import com.epam.nikitina.elib.database.entities.JournalEntity;

import java.util.ArrayList;
import java.util.List;

public class User {
    private DAOFactory factory;

    public User() {
        factory = DAOFactory.getInstance();
    }

    public void requestBook(String login, int code, String hold) {
        factory.getManageDAO().requestBook(login, code, hold);
    }

    public void cancelRequest(int id) {
        factory.getManageDAO().rejectRequest(id);
    }

    public void updateHold(int id, String hold) {
        factory.getJournalDAO().updateHold(id, hold);
    }


    public ArrayList<JournalBean> getUserNotes(String login, int offset, int count) {
        List<JournalEntity> notes = factory.getJournalDAO().getByUser(login, offset, count);

        return toBeanList(notes);
    }

    private ArrayList<JournalBean> toBeanList(List<JournalEntity> entities) {
        ArrayList<JournalBean> beans = new ArrayList<>();
        for (JournalEntity note : entities) {
            beans.add(new JournalBean(note));
        }
        return beans;
    }

    public Integer getNotesCount(String login) {
        return factory.getJournalDAO().getUserCount(login);
    }
}
