package com.epam.nikitina.elib.api;

import com.epam.nikitina.elib.beans.UserBean;
import com.epam.nikitina.elib.database.dao.DAOFactory;

import java.util.Date;

public class Admin {
    private DAOFactory factory;

    public Admin() {
        factory = DAOFactory.getInstance();
    }

    public UserBean getUserInfo(String login, String password) {
        if (factory.getAdminDAO().validate(login, password) == true) {
            return factory.getReaderDAO().getUser(login);
        }
        return null;
    }

    public boolean checkLogin(String login) {
        return factory.getAdminDAO().isLoginFree(login);
    }

    public boolean registerUser(String login, String password, String first, String last, Date birth) {
        return factory.getAdminDAO().registerUser(login, password, first, last, birth);
    }
}
