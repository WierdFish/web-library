package com.epam.nikitina.elib.services.controller;

import com.epam.nikitina.elib.api.User;
import com.epam.nikitina.elib.beans.JournalBean;
import com.epam.nikitina.elib.beans.UserBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;


public class CabinetController implements Controller {
    private final Logger logger = LogManager.getLogger(CabinetController.class);
    private User user = new User();

    @Override
    public void handleGetRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        HttpSession session = request.getSession();
        UserBean userBean = (UserBean) session.getAttribute("userData");
        int count = 5;
        try {
            int pages = user.getNotesCount(userBean.getLogin());
            String page = request.getParameter("page");
            int maxPages = pages / count + (((pages % count) == 0) ? 0 : 1);
            int offset = 0;
            int curPage = 1;

            if (page != null) {
                offset = (Integer.parseInt(page) - 1) * count;
                curPage = Integer.parseInt(page);
            }
            if ((curPage > maxPages) && (maxPages != 0) || curPage <= 0) throw new IllegalStateException();
            List<JournalBean> notes = user.getUserNotes(userBean.getLogin(), offset, count);
            request.setAttribute("notes", notes);
            request.setAttribute("pages", maxPages);
            logger.info(userBean.getLogin() + " Open cabinet. Pages: " + pages);
            RequestDispatcher rd = context.getRequestDispatcher("/personal/cabinet.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
            logger.info(userBean.getLogin() + " Invalid cabinet page request: ", e);
            response.sendRedirect("/app/personal/cabinet");
        }

    }

    @Override
    public void handlePostRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        HttpSession session = request.getSession();
        UserBean userBean = (UserBean) session.getAttribute("userData");

        String action = request.getParameter("action");
        int id = Integer.parseInt(request.getParameter("id"));
        if (action.equals("add")) {
            String hold = request.getParameter("hold");
            user.requestBook(userBean.getLogin(), id, hold);
            logger.info(userBean.getLogin() + " request book. id:" + id + " hold:" + hold);
        } else if (action.equals("update")) {
            String hold = request.getParameter("hold");
            user.updateHold(id, hold);
            logger.info(userBean.getLogin() + " update hold. Note id:" + id + " hold:" + hold);

        } else if (action.equals("delete")) {
            user.cancelRequest(id);
            logger.info(userBean.getLogin() + " cancel request: id" + id);
        }
        response.sendRedirect("/app/personal/cabinet");

    }
}
