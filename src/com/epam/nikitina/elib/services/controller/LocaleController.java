package com.epam.nikitina.elib.services.controller;

import com.epam.nikitina.elib.beans.UserBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

public class LocaleController implements Controller {
    private final Logger logger = LogManager.getLogger(LocaleController.class);

    @Override
    public void handleGetRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        String locale = request.getParameter("value");
        HttpSession session = request.getSession();
        UserBean user = ((UserBean) session.getAttribute("userData"));
        String name;
        if (user!=null){
            name = user.getLogin();
        } else {
            name = "Guest";
        }
        if (locale.equals("ru")) {
            Config.set(request.getSession(), Config.FMT_LOCALE, new java.util.Locale("ru", "RU"));
            logger.info(name + " set locale ru");
        } else if (locale.equals("en")) {
            Config.set(request.getSession(), Config.FMT_LOCALE, new java.util.Locale("en"));
            logger.info(name + " set locale en");
        }
        String url = request.getHeader("referer");
        response.sendRedirect(url);
    }

    @Override
    public void handlePostRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {

    }
}
