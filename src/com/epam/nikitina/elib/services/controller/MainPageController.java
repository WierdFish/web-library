package com.epam.nikitina.elib.services.controller;

import com.epam.nikitina.elib.api.BookList;
import com.epam.nikitina.elib.beans.BooksBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

public class MainPageController implements Controller {
    private final Logger logger = LogManager.getLogger(MainPageController.class);

    @Override
    public void handleGetRequest(HttpServletRequest request, HttpServletResponse response, final ServletContext context) throws Exception {

        BookList bl = new BookList();
        try {
            int count = 5;
            int pages = bl.getBooksCount();
            String page = request.getParameter("page");
            int maxPages = pages / count + (((pages % count) == 0) ? 0 : 1);

            int offset = 0;
            int curPage = 1;

            if (page != null) {
                offset = (Integer.parseInt(page) - 1) * count;
                curPage = Integer.parseInt(page);
            }
            if (curPage > maxPages || curPage <= 0) throw new Exception();

            ArrayList<BooksBean> bookPage = bl.getPage(offset, count);

            request.setAttribute("books", bookPage);
            request.setAttribute("pages", maxPages);

            RequestDispatcher rd = context.getRequestDispatcher("/index.jsp");
            rd.forward(request, response);

        } catch (Exception e) {
            logger.info("Invalid book page request: ", e);
            response.sendRedirect("/app/index");
        }

    }

    @Override
    public void handlePostRequest(HttpServletRequest request, HttpServletResponse response, final ServletContext context) throws Exception {

    }
}
