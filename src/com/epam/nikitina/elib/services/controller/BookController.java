package com.epam.nikitina.elib.services.controller;

import com.epam.nikitina.elib.api.BookList;
import com.epam.nikitina.elib.api.Librarian;
import com.epam.nikitina.elib.beans.BooksBean;
import com.epam.nikitina.elib.beans.UserBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class BookController implements Controller {
    private final Logger logger = LogManager.getLogger(BookController.class);
    private BookList bookList = new BookList();
    private Librarian lib = new Librarian();

    @Override
    public void handleGetRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        UserBean user = (UserBean) session.getAttribute("userData");
        String login = user.getLogin();

        if (action == null || action.equals("add")) {
            BooksBean book = new BooksBean();
            book.setYear(1970);
            request.setAttribute("book", book);
            logger.info(login + "Opened book manage page (add)");
            RequestDispatcher rd = context.getRequestDispatcher("/admin/book.jsp");
            rd.forward(request, response);
            return;
        } else if (action.equals("update")) {
            String str_id = request.getParameter("id");
            int id = Integer.parseInt(str_id);

            try {
                BooksBean book = bookList.getBook(id);
                request.setAttribute("book", book);
                logger.info(login + "Opened book manage page (update id:" + id + ")");
                RequestDispatcher rd = context.getRequestDispatcher("/admin/book.jsp");
                rd.forward(request, response);
            } catch (Exception e) {
                logger.info(login + "Can't open manage page. Invalid request: ", e);
                response.sendRedirect("/app/index");
            }
        }
    }

    @Override
    public void handlePostRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        UserBean user = (UserBean) session.getAttribute("userData");
        String login = user.getLogin();

        if (action.equals("add")) {
            add(request, response, context, login);
        } else if (action.equals("delete")) {
            delete(request, response, context, login);
        } else if (action.equals("update")) {
            update(request, response, context, login);
        }

    }

    private void add(HttpServletRequest request, HttpServletResponse response, ServletContext context, String login) throws ServletException, IOException {
        request.setAttribute("action", request.getParameter("action"));
        RequestDispatcher rd = context.getRequestDispatcher("/admin/errpage.jsp");

        try {
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String title = request.getParameter("title");
            String strYear = request.getParameter("year");
            if (strYear.length() > 4) throw new Exception();
            int year = Integer.parseInt(strYear);
            if (year <= 0) {
                logger.info(login + " Incorrect book year: " + year);
                request.setAttribute("success", false);
                return;
            }
            int amount = Integer.parseInt(request.getParameter("amount"));
            if (amount < 0) {
                logger.info(login + " Incorrect book amount: " + amount);
                request.setAttribute("success", false);
                return;
            }
            lib.addBook(firstName, lastName, title, year, amount);
            request.setAttribute("success", true);
            logger.info(login + "add new book:" + firstName + " " + lastName + " " + title + " " + strYear + " " + amount);

        } catch (Exception e) {
            logger.info(login + "Can't add book. Incorrect user data: ", e);
            request.setAttribute("success", false);

        } finally {
            rd.forward(request, response);
        }
    }

    private void update(HttpServletRequest request, HttpServletResponse response, ServletContext context, String login) throws ServletException, IOException {

        request.setAttribute("action", request.getParameter("action"));
        RequestDispatcher rd = context.getRequestDispatcher("/admin/errpage.jsp");
        try {
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String title = request.getParameter("title");
            String strYear = request.getParameter("year");
            if (strYear.length() > 4) throw new Exception();
            int year = Integer.parseInt(strYear);
            if (year <= 0) {
                logger.info(login + " Incorrect book year: " + year);
                request.setAttribute("success", false);
                return;
            }
            int amount = Integer.parseInt(request.getParameter("amount"));
            if (amount < 0) {
                logger.info(login + " Incorrect book amount: " + amount);
                request.setAttribute("success", false);
                return;
            }
            int id = Integer.parseInt(request.getParameter("id"));
            lib.updateBook(id, firstName, lastName, title, year, amount);
            request.setAttribute("success", true);
            logger.info(login + "updated book id:" + id + " " + firstName + " " + lastName + " " + title + " " + strYear + " " + amount);
        } catch (Exception e) {
            logger.info(login + " Can't update book. Incorrect user data: ", e);
            request.setAttribute("success", false);
        } finally {
            rd.forward(request, response);
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response, ServletContext context, String login) throws ServletException, IOException {
        request.setAttribute("action", request.getParameter("action"));
        RequestDispatcher rd = context.getRequestDispatcher("/admin/errpage.jsp");
        try {
            lib.removeBook(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("success", true);
            logger.info(login + " delete book:" + request.getParameter("id"));
        } catch (Exception e) {
            logger.info(login + " Failed to delete: ", e);
            request.setAttribute("success", false);
        } finally {
            rd.forward(request, response);
        }
    }


}
