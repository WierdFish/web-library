package com.epam.nikitina.elib.services.controller;

import com.epam.nikitina.elib.api.Librarian;
import com.epam.nikitina.elib.beans.JournalBean;
import com.epam.nikitina.elib.beans.UserBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ManageController implements Controller {
    private final Logger logger = LogManager.getLogger(ManageController.class);
    private Librarian lib = new Librarian();

    @Override
    public void handleGetRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        HttpSession session = request.getSession();
        try {
            int pages = lib.getNotesCount();
            int count = 10;
            String page = request.getParameter("page");
            int maxPages = pages / count + (((pages % count) == 0) ? 0 : 1);
            int offset = 0;
            int curPage = 1;

            if (page != null) {
                offset = (Integer.parseInt(page) - 1) * count;
                curPage = Integer.parseInt(page);
            }
            if ((curPage > maxPages) && (maxPages != 0) || curPage <= 0)
                throw new IllegalStateException("Page out of bounds");

            List<JournalBean> notes = lib.getNotesPage(offset, count);
            request.setAttribute("notes", notes);
            request.setAttribute("pages", maxPages);

            RequestDispatcher rd = context.getRequestDispatcher("/admin/panel.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
            logger.warn("Invalid manage page request: ", e);
            response.sendRedirect("/app/admin/panel");
        }

    }

    @Override
    public void handlePostRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        HttpSession session = request.getSession();
        String user = ((UserBean) session.getAttribute("userData")).getLogin();

        String action = request.getParameter("action");
        int id = Integer.parseInt(request.getParameter("id"));
        if (action.equals("reject")) {
            lib.rejectRequest(id);
            logger.info(user + " reject request id:" + id);
        } else if (action.equals("accept")) {
            lib.acceptRequest(id);
            logger.info(user + " accept request id:" + id);

        } else {
            lib.confirmReturn(id);
            logger.info(user + " confirm return request id:" + id);

        }
        response.sendRedirect("/app/admin/panel");

    }
}
