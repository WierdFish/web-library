package com.epam.nikitina.elib.services.controller;

import com.epam.nikitina.elib.api.Admin;
import com.epam.nikitina.elib.beans.UserBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RegistrationController implements Controller {
    private Admin admin = new Admin();
    private final Logger logger = LogManager.getLogger(RegistrationController.class);

    @Override
    public void handleGetRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        RequestDispatcher rd = context.getRequestDispatcher("/registration.jsp");
        rd.forward(request, response);
    }

    @Override
    public void handlePostRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        HttpSession session = request.getSession();

        String login = request.getParameter("login");
        if (admin.checkLogin(login)) {
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String password = request.getParameter("password");
            String date = request.getParameter("birthDate");
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date birthDate = format.parse(date);

            if (admin.registerUser(login, password, firstName, lastName, birthDate)) {
                UserBean userdata = admin.getUserInfo(login, password);
                session.setAttribute("userData", userdata);
                logger.info("Successful registration: " + login + " " + firstName + " " + lastName + " " + birthDate);
                response.sendRedirect("/app/personal/cabinet");
            }
        } else {
            request.setAttribute("regErr", true);
            logger.info("Failed to register user. Login: " + login);
            RequestDispatcher rd = context.getRequestDispatcher("/registration.jsp");
            rd.forward(request, response);
        }

    }
}
