package com.epam.nikitina.elib.services.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Controller {

    void handleGetRequest(final HttpServletRequest request, final HttpServletResponse response, final ServletContext context) throws Exception;

    void handlePostRequest(final HttpServletRequest request, final HttpServletResponse response, final ServletContext context) throws Exception;
}
