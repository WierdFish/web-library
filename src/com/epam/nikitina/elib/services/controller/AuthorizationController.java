package com.epam.nikitina.elib.services.controller;

import com.epam.nikitina.elib.api.Admin;
import com.epam.nikitina.elib.beans.UserBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthorizationController implements Controller {
    private Admin admin = new Admin();
    private final Logger logger = LogManager.getLogger(AuthorizationController.class);

    @Override
    public void handleGetRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        if (request.getParameter("log") != null) {
            response.sendRedirect("/app/index");
            HttpSession session = request.getSession(false);
            logger.info("User " + ((UserBean) session.getAttribute("userData")).getLogin() + " logged out");
            session.invalidate();
            session = request.getSession(false);
        } else {
            RequestDispatcher rd = context.getRequestDispatcher("/authorization.jsp");
            rd.forward(request, response);
        }
    }

    @Override
    public void handlePostRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        HttpSession session = request.getSession();

        String login = request.getParameter("login").toString();
        String password = request.getParameter("password").toString();
        UserBean userdata = admin.getUserInfo(login, password);

        if (userdata == null) {
            request.setAttribute("authErr", true);
            logger.info("Authorization failed: incorrect data");
            RequestDispatcher rd = context.getRequestDispatcher("/authorization.jsp");
            rd.forward(request, response);
        } else {
            session.setAttribute("userData", userdata);
            if (!userdata.isAdmin()) {
                logger.info("User " + userdata.getLogin() + " authorized");
                response.sendRedirect("/app/personal/cabinet");
            } else {
                logger.info("Admin " + userdata.getLogin() + " authorized");
                response.sendRedirect("/app/admin/panel");

            }
        }
    }
}
