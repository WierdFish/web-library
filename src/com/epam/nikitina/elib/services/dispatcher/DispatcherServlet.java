package com.epam.nikitina.elib.services.dispatcher;

import com.epam.nikitina.elib.beans.UserBean;
import com.epam.nikitina.elib.services.controller.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DispatcherServlet extends HttpServlet {
    protected Map<String, Controller> controllerMap = new HashMap<>();
    private final Logger logger = LogManager.getLogger(DispatcherServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Controller controller = getController(req.getRequestURI());
        UserBean user = (UserBean) req.getSession().getAttribute("userData");
        String login;
        if (user != null) {
            login = user.getLogin();
        } else {
            login = "Guest";
        }
        logger.info(login + " requested url: " + req.getRequestURI());
        if (controller != null) {
            try {
                controller.handleGetRequest(req, resp, getServletContext());
            } catch (Exception e) {
                logger.error(" Error while " + req.getRequestURI(), e);
                resp.sendRedirect("/app/index");
            }
        } else {
            logger.info("Unknown page requested " + req.getRequestURI());
            resp.sendRedirect("/app/index");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        UserBean user = (UserBean) req.getSession().getAttribute("userData");
        String login;
        if (user != null) {
            login = user.getLogin();
        } else {
            login = "Guest";
        }
        logger.info(login + " requested url: " + req.getRequestURI());
        Controller controller = getController(req.getRequestURI());
        if (controller != null) {
            try {
                controller.handlePostRequest(req, resp, getServletContext());
            } catch (Exception e) {
                logger.error("Error while " + req.getRequestURI(), e);
                resp.sendRedirect("/app/index");
            }
        } else {
            logger.info("Unknown page requested " + req.getRequestURI());
            resp.sendRedirect("/app/index");
        }
    }

    private Controller getController(String url) {
        for (String key : controllerMap.keySet()) {
            if (url.contains(key)) {
                return controllerMap.get(key);
            }
        }
        return null;
    }

    @Override
    public void init() throws ServletException {
        MainPageController mainPageController = new MainPageController();
        controllerMap.put("/index", mainPageController);
        AuthorizationController authorizationController = new AuthorizationController();
        controllerMap.put("/authorization", authorizationController);
        CabinetController cabinetController = new CabinetController();
        controllerMap.put("/personal/cabinet", cabinetController);
        ManageController manageController = new ManageController();
        controllerMap.put("/admin/panel", manageController);
        RegistrationController registrationController = new RegistrationController();
        controllerMap.put("/registration", registrationController);
        BookController bookController = new BookController();
        controllerMap.put("/admin/book", bookController);
        LocaleController localeController = new LocaleController();
        controllerMap.put("/locale", localeController);
    }
}
