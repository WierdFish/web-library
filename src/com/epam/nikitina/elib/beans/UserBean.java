package com.epam.nikitina.elib.beans;

import com.epam.nikitina.elib.database.entities.ReadersEntity;

import java.io.Serializable;
import java.util.Date;

public class UserBean implements Serializable {
    private String login;
    private String name;
    private Date birthDate;
    private Date registryDate;
    boolean isAdmin;

    public boolean isAdmin() {
        return isAdmin;
    }

    public UserBean() {
        isAdmin = false;
    }

    public UserBean(ReadersEntity rd) {
        login = rd.getLogin();
        name = rd.getFirstName() + Character.valueOf(' ') + rd.getLastName();
        birthDate = rd.getBirthDate();
        registryDate = rd.getRegistryDate();
        isAdmin = rd.getLoginDataEntity().isAdmin();
    }

    public UserBean(String login, String name, Date birth, Date registry) {
        this.login = login;
        this.name = name;
        this.birthDate = birth;
        this.registryDate = registry;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }
}
