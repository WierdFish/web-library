package com.epam.nikitina.elib.beans;

import com.epam.nikitina.elib.api.BookList;
import com.epam.nikitina.elib.database.entities.BooksEntity;

import java.io.Serializable;

public class BooksBean implements Serializable {
    private int id;
    private String author;
    private String title;
    private String firstName;
    private String lastName;
    private int year;
    private int amount;

    public BooksBean(int id, String firstName, String lastName, String title, int year, int amount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.author = firstName + Character.valueOf(' ') + lastName;
        this.title = title;
        this.year = year;
        this.amount = amount;
    }

    public BooksBean(BooksEntity bk) {
        this.id = bk.getCode();
        this.author = bk.getAuthorFirstName() + Character.valueOf(' ') + bk.getAuthorLastName();
        this.firstName = bk.getAuthorFirstName();
        this.lastName = bk.getAuthorLastName();
        this.title = bk.getTitle();
        this.year = bk.getYear();
        this.amount = bk.getAmount();
    }

    public BooksBean() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
