package com.epam.nikitina.elib.beans;

import com.epam.nikitina.elib.database.entities.JournalEntity;

import java.io.Serializable;
import java.sql.Timestamp;

public class JournalBean implements Serializable {
    private int id;
    private String login;
    private int book;
    private String bookAuthor;
    private String bookTitle;
    private Timestamp reserveDate;
    private Timestamp returnDate;
    private String hold;
    private Boolean accepted;

    public JournalBean(int id, String login, int book, String bookAuthor, String bookTitle, Timestamp reserveDate, Timestamp returnDate, String hold, Boolean accepted) {
        this.id = id;
        this.login = login;
        this.book = book;
        this.bookAuthor = bookAuthor;
        this.bookTitle = bookTitle;
        this.reserveDate = reserveDate;
        this.returnDate = returnDate;
        this.hold = hold;
        this.accepted = accepted;
    }

    public JournalBean() {
        accepted = false;
    }

    public JournalBean(JournalEntity je) {
        this.id = je.getNoteId();
        this.login = je.getReadersEntity().getLogin();
        this.book = je.getBooksEntity().getCode();
        this.reserveDate = je.getReserveDate();
        this.returnDate = je.getReturnDate();
        this.hold = je.getHold();
        this.accepted = je.isAccepted();
        this.bookAuthor = je.getBooksEntity().getAuthorFirstName() + Character.valueOf(' ') + je.getBooksEntity().getAuthorLastName();
        this.bookTitle = je.getBooksEntity().getTitle();
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getBook() {
        return book;
    }

    public void setBook(int book) {
        this.book = book;
    }

    public Timestamp getReserveDate() {
        return reserveDate;
    }

    public void setReserveDate(Timestamp reserveDate) {
        this.reserveDate = reserveDate;
    }

    public Timestamp getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Timestamp returnDate) {
        this.returnDate = returnDate;
    }

    public String getHold() {
        return hold;
    }

    public void setHold(String hold) {
        this.hold = hold;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }
}
