package com.epam.nikitina.elib.taglib;


import com.epam.nikitina.elib.beans.UserBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class AuthLogTag extends TagSupport {
    private final Logger logger = LogManager.getRootLogger();

    @Override
    public int doStartTag() throws JspException {
        HttpSession session = pageContext.getSession();
        JspWriter out = pageContext.getOut();
        if (session.getAttribute("userData") != null) {
            try {
                UserBean user = (UserBean) session.getAttribute("userData");
                out.write(user.getLogin());
            } catch (IOException e) {
                logger.error("Can't write username: ", e);

            }
        }
        return SKIP_BODY;
    }

}
