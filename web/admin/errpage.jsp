<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<link rel='stylesheet' href='<c:url value = "/styles/blocks.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/bookBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/links.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/filterBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/classes.css"/>'>
<script type="text/javascript" src='<c:url value = "/javascript/jquery-3.1.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/functions.js"/>'></script>
<fmt:setBundle basename="resources" var="label"/>
<body background=<c:url value="/images/bg.png"/>>
<div id="container">
    <jsp:include page="/header.jsp"></jsp:include>

    <div id="bookEdit">
        <c:if test="${requestScope.success eq false}">
            <p class="error"><fmt:message key="bookErr" bundle="${label}"/></p>
        </c:if>
        <c:if test="${requestScope.success eq true}">
            <c:if test="${requestScope.action eq 'add'}">
                <p class="success"><fmt:message key="addSuccess" bundle="${label}"/></p>
            </c:if>
            <c:if test="${requestScope.action eq 'update'}">
                <p class="success"><fmt:message key="updSuccess" bundle="${label}"/></p>
            </c:if>
            <c:if test="${requestScope.action eq 'delete'}">
                <p class="success"><fmt:message key="delSuccess" bundle="${label}"/></p>
            </c:if>
        </c:if>
    </div>

</div>

</body>
</html>
