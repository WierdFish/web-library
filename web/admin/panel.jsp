<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<html>
<link rel='stylesheet' href='<c:url value = "/styles/blocks.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/bookBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/links.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/filterBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/classes.css"/>'>
<script type="text/javascript" src='<c:url value = "/javascript/jquery-3.1.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/functions.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/jquery-3.1.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/functions.js"/>'></script>
<fmt:setBundle basename="resources" var="label"/>
<body background=<c:url value="/images/bg.png"/>>
<div id="container">
    <jsp:include page="/header.jsp"></jsp:include>

    <div id="content">
        <div id="buttons">
            <a id="editBook"  href="/app/admin/book?action=add" class="reserveButton">
                <fmt:message key="buttonAdd" bundle="${label}"/>
            </a>
        </div>
        <div id="controlTable" class="lightPanel">
            <table class="notesHist">
                <tr class="space">
                    <td>
                        <span class="tableTitle"> <fmt:message key="tableUser" bundle="${label}"/> </span>
                    </td>
                    <td>
                        <span class="tableTitle"> <fmt:message key="tableBook" bundle="${label}"/> </span>
                    </td>
                    <td>
                        <span class="tableTitle"><fmt:message key="tableHoldDate" bundle="${label}"/></span>
                    </td>
                    <td>
                        <span class="tableTitle"><fmt:message key="tableHoldType" bundle="${label}"/></span>
                    </td>
                    <td>
                        <span class="tableTitle"><fmt:message key="tableAccepted" bundle="${label}"/></span>
                    </td>
                    <td>
                        <span class="tableTitle"><fmt:message key="tableDateReturn" bundle="${label}"/></span>
                    </td>

                </tr>
                <c:forEach var="item" items="${requestScope.notes}">
                    <tr class="space">
                        <td>
                                ${item.getLogin()}
                        </td>
                        <td>
                            <span class="boldTitle">${item.getBookTitle()} </span>
                            </br>${item.getBookAuthor()}
                        </td>
                        <td>
                                ${item.getReserveDate()}
                        </td>
                        <td>
                            <c:if test="${item.getHold() eq 'home'}">
                                <fmt:message key="homeHold" bundle="${label}"/>
                            </c:if>
                            <c:if test="${item.getHold() eq 'hall'}">
                                <fmt:message key="hallHold" bundle="${label}"/>
                            </c:if>

                        </td>
                        <td>
                            <c:if test="${item.getAccepted() eq false}">
                                <form id="rejectForm${item.getId()}" action="/app/admin/panel" method="POST">
                                    <input name="id" type="hidden" value="${item.getId()}">
                                    <input name="action" type="hidden" value="reject">
                                    <input class="buttonInput" type="submit"
                                           value="<fmt:message key="buttonReject" bundle="${label}"/>">
                                </form>
                                <form id="acceptForm${item.getId()}" action="/app/admin/panel" method="POST">
                                    <input name="id" type="hidden" value="${item.getId()}">
                                    <input name="action" type="hidden" value="accept">
                                    <input class="buttonInput" type="submit"
                                           value="<fmt:message key="buttonAccept" bundle="${label}"/>">
                                </form>
                            </c:if>
                            <c:if test="${item.getAccepted() eq true}">
                                <fmt:message key="labelAccepted" bundle="${label}"/>
                            </c:if>
                        </td>
                        <td>
                            <c:if test="${item.getReturnDate() ne null}">
                                ${item.getReturnDate()}
                            </c:if>
                            <c:if test="${item.getReturnDate() eq null && item.getAccepted() eq true}">
                                <form id="acceptForm${item.getId()}" action="/app/admin/panel" method="POST">
                                    <input name="id" type="hidden" value="${item.getId()}">
                                    <input name="action" type="hidden" value="confirm">
                                    <input class="buttonInput" type="submit"
                                           value="<fmt:message key="buttonConfirmReturn" bundle="${label}"/>">
                                </form>
                            </c:if>

                        </td>
                    </tr>

                </c:forEach>
            </table>
            <div id="pages">
                <c:forEach begin="1" end="${requestScope.pages}" varStatus="loop">
                    <a class="page" href='<c:url value = "/app/admin/panel?page=${loop.index}"/>'>${loop.index}</a>
                </c:forEach>
            </div>
        </div>
    </div>

</div>

</body>
</html>
