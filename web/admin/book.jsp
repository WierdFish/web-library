<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<link rel='stylesheet' href='<c:url value = "/styles/blocks.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/bookBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/links.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/filterBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/classes.css"/>'>
<script type="text/javascript" src='<c:url value = "/javascript/jquery-3.1.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/functions.js"/>'></script>
<fmt:setBundle basename="resources" var="label"/>
<body background=<c:url value="/images/bg.png"/>>
<div id="container">
    <jsp:include page="/header.jsp"></jsp:include>

    <div id="bookEdit">

        <form action="/app/admin/book" method="POST" accept-charset="UTF-8">
            <c:if test="${requestScope.updSuccess eq true}">
                <p class="success"><fmt:message key="updSuccess" bundle="${label}"/></p>
            </c:if>
            <c:if test="${requestScope.addSuccess eq true}">
                <p class="success"><fmt:message key="addSuccess" bundle="${label}"/></p>
            </c:if>
            <c:if test="${requestScope.err eq true}">
                <p class="error"><fmt:message key="bookErr" bundle="${label}"/></p>
            </c:if>
            <span class="label"><fmt:message key="authorLabel" bundle="${label}"/></span>
            <input name="firstName" class="login" type="text" size="45" required
                   value="${requestScope.book.getFirstName()}"
                   placeholder="<fmt:message key="hintFirstName" bundle="${label}"/>"/>
            </br>
            <input name="lastName" class="login" type="text" size="45" required
                   value="${requestScope.book.getLastName()}"
                   placeholder="<fmt:message key="hintLastName" bundle="${label}"/>"/>
            </br>
            <span class="label"><fmt:message key="titleLabel" bundle="${label}"/></span>
            <input name="title" class="login" type="text" size="45" required value="${requestScope.book.getTitle()}"/>
            </br>
            <span class="label"><fmt:message key="yearLabel" bundle="${label}"/></span>
            <input name="year" class="login" type="text" min="1"  size="45" required value="${requestScope.book.getYear()}"/>
            </br>
            <span class="label"><fmt:message key="amountLabel" bundle="${label}"/></span>
            <input name="amount" class="login" type="number" min="0" required value="${requestScope.book.getAmount()}"/>
            </br>
            <c:if test="${param.action eq 'add'}">
                <input type="hidden" name="action" value="add">
                <input class="buttonInput" type="submit"
                       value="<fmt:message key="buttonAdd" bundle="${label}"/>">
            </c:if>
            <c:if test="${param.action eq 'update'}">
                <input type="hidden" name="action" value="update">
                <input type="hidden" name="id" value="${requestScope.book.getId()}">
                <input class="buttonInput" type="submit"
                       value="<fmt:message key="listButtonUpdate" bundle="${label}"/>">
            </c:if>
        </form>

    </div>

</div>

</body>
</html>
