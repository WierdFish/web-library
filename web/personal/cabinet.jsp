<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<link rel='stylesheet' href='<c:url value = "/styles/blocks.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/bookBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/links.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/filterBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/classes.css"/>'>
<script type="text/javascript" src='<c:url value = "/javascript/jquery-3.1.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/functions.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/jquery-3.1.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/functions.js"/>'></script>
<fmt:setBundle basename="resources" var="label"/>
<body background=<c:url value="/images/bg.png"/>>
<div id="container">
    <jsp:include page="/header.jsp"></jsp:include>
    <div id="content">
        <div id="userInfo" class="lightPanel">
            <p><span class="boldTitle"> <fmt:message key="cabName" bundle="${label}"/></span>
                ${sessionScope.userData.getName()}</p>
            <p><span class="boldTitle"><fmt:message key="cabBirth" bundle="${label}"/> </span>
                ${sessionScope.userData.getBirthDate()}</p>
            <p><span class="boldTitle"> <fmt:message key="cabReg" bundle="${label}"/></span>
                ${sessionScope.userData.getRegistryDate()}</p>
        </div>

        <div id="table" class="lightPanel">
            <table class="notesHist">
                <tr class="space">
                    <td>
                        <span class="tableTitle"> <fmt:message key="tableBook" bundle="${label}"/> </span>
                    </td>
                    <td>
                        <span class="tableTitle"><fmt:message key="tableHoldDate" bundle="${label}"/></span>
                    </td>
                    <td>
                        <span class="tableTitle"><fmt:message key="tableHoldType" bundle="${label}"/></span>
                    </td>
                    <td>
                        <span class="tableTitle"><fmt:message key="tableAccepted" bundle="${label}"/></span>
                    </td>
                    <td>
                        <span class="tableTitle"><fmt:message key="tableDateReturn" bundle="${label}"/></span>
                    </td>
                    <td>
                    </td>
                </tr>
                <c:forEach var="item" items="${requestScope.notes}">
                    <tr class="space">
                        <td>
                            <span class="boldTitle">${item.getBookTitle()} </span>
                            </br>${item.getBookAuthor()}
                        </td>
                        <td>
                                ${item.getReserveDate()}
                        </td>
                        <td>
                            <c:if test="${item.getHold() eq 'home'}">
                                <fmt:message key="homeHold" bundle="${label}"/>
                            </c:if>
                            <c:if test="${item.getHold() eq 'hall'}">
                                <fmt:message key="hallHold" bundle="${label}"/>
                            </c:if>

                        </td>
                        <td>
                            <c:if test="${item.getAccepted() eq false}">
                                <fmt:message key="labelWait" bundle="${label}"/>
                            </c:if>
                            <c:if test="${item.getAccepted() eq true}">
                                <fmt:message key="labelAccepted" bundle="${label}"/>
                            </c:if>
                        </td>
                        <td>
                            <c:if test="${item.getReturnDate() ne null}">
                                ${item.getReturnDate()}
                            </c:if>
                        </td>
                        <td>
                            <c:if test="${item.getAccepted() eq false}">
                                <form id="reserveForm${item.getId()}" hidden action="/app/personal/cabinet" method="POST">
                                    <input name="id" type="hidden" value="${item.getId()}">
                                    <input name="action" type="hidden" value="update">
                                    <select name="hold">
                                        <option value="home"><fmt:message key="homeHold" bundle="${label}"/></option>
                                        <option value="hall"><fmt:message key="hallHold" bundle="${label}"/></option>
                                    </select>
                                    <input class="buttonInput" type="submit"
                                           value="<fmt:message key="buttonConfirm" bundle="${label}"/>">
                                </form>

                                <button id="updButton${item.getId()}" class="listButton" onclick="showFormUpdate(${item.getId()})">
                                    <fmt:message key="listButtonUpdate" bundle="${label}"/>
                                </button>

                                <form id="deleteForm${item.getId()}" action="/app/personal/cabinet" method="POST">
                                    <input name="id" type="hidden" value="${item.getId()}">
                                    <input name="action" type="hidden" value="delete">
                                    <input class="buttonInput" type="submit"
                                           value="<fmt:message key="listButtonCancel" bundle="${label}"/>">
                                </form>
                            </c:if>
                        </td>
                    </tr>

                </c:forEach>
            </table>
            <div id="pages">
                <c:forEach begin="1" end="${requestScope.pages}" varStatus="loop">
                    <a class="page" href='<c:url value = "/app/personal/cabinet?page=${loop.index}"/>'>${loop.index}</a>
                </c:forEach>
            </div>
        </div>

    </div>

</div>

</body>
</html>
