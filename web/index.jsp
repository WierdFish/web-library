<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mytag" uri="/WEB-INF/tld/mytaglib.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<link rel='stylesheet' href='<c:url value = "/styles/blocks.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/bookBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/links.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/filterBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/classes.css"/>'>
<script type="text/javascript" src='<c:url value = "/javascript/jquery-3.1.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/functions.js"/>'></script>

<fmt:setBundle basename="resources" var="label"/>

<body background=<c:url value="/images/bg.png"/>>
<div id="container">
<jsp:include page="header.jsp"></jsp:include>

    <div id="content">
    <!--    <div id="filters">
            <details>

                <summary><fmt:message key="filterHeader" bundle="${label}"/></summary>
                <div id="filterPanel">
                    <p class="label"><fmt:message key="authorLabel" bundle="${label}"/></p>
                    <input class="searchLine" type="text" size="45"
                           placeholder="<fmt:message key="hintFirstName" bundle="${label}"/>"/>
                    <input class="searchLine" type="text" size="45"
                           placeholder="<fmt:message key="hintLastName" bundle="${label}"/>"/>

                    <p class="label"><fmt:message key="titleLabel" bundle="${label}"/></p>
                    <input class="searchLine" type="text" size="45"/>

                    <p class="label"><fmt:message key="yearLabel" bundle="${label}"/></p>
                    <input class="searchLine" type="text" size="45"/>
                </div>
            </details>
        </div> -->

        <div id="list">

            <c:forEach var="item" items="${requestScope.books}">
                <div id="bookElem" class="lightPanel">
                    <div id="bookInfo">
                        <p id="bookText">
                            <span class="boldTitle">${item.getTitle()}</span></br>
                            <span class="lightText">${item.getAuthor()}, ${item.getYear()}
                        </p> </span>

                    </div>

                    <div id="reserveInfo" align="right">
                        <p><fmt:message key="available" bundle="${label}"/> ${item.getAmount()}</p>
                        <c:if test="${sessionScope.userData ne null}">
                            <c:if test="${sessionScope.userData.isAdmin() eq true}">

                                <a id="editBook" href="/app/admin/book?action=update&id=${item.getId()}"
                                   class="reserveButton">
                                    <fmt:message key="buttonEdit" bundle="${label}"/>
                                </a>

                            </c:if>

                            <c:if test="${sessionScope.userData.isAdmin() eq false}">
                                <c:if test="${item.getAmount() ne 0}">
                                    <button id="showForm${item.getId()}" onclick="showForm(${item.getId()})"
                                            class="reserveButton">
                                        <fmt:message key="reserve" bundle="${label}"/>
                                    </button>
                                    <form id="reserveForm${item.getId()}" hidden action="/app/personal/cabinet"
                                          method="POST">
                                        <input name="id" type="hidden" value="${item.getId()}">
                                        <input name="action" type="hidden" value="add">
                                        <select name="hold">
                                            <option value="home"><fmt:message key="homeHold"
                                                                              bundle="${label}"/></option>
                                            <option value="hall"><fmt:message key="hallHold"
                                                                              bundle="${label}"/></option>
                                        </select>
                                        <input class="buttonInput" type="submit"
                                               value="<fmt:message key="buttonConfirm" bundle="${label}"/>">
                                    </form>
                                </c:if>
                            </c:if>
                        </c:if>
                    </div>
                    <c:if test="${sessionScope.userData.isAdmin() eq true}">
                        <div id="removeBlock">

                            <form action="/app/admin/book" method="POST" accept-charset="UTF-8">
                                <input type="hidden" name="action" value="delete">
                                <input type="hidden" name="id" value="${item.getId()}">
                                <input class="buttonInput" type="submit"
                                       value="<fmt:message key="bookDelete" bundle="${label}"/>">
                            </form>

                        </div>
                    </c:if>

                </div>
            </c:forEach>

            <div id="pages">
                <c:forEach begin="1" end="${requestScope.pages}" varStatus="loop">
                  <a class="page" href='<c:url value = "/app/index?page=${loop.index}"/>'>${loop.index}</a>
                </c:forEach>
            </div>
        </div>

    </div>

</div>

</body>
</html>
