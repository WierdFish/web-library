<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mytag" uri="/WEB-INF/tld/mytaglib.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<link rel='stylesheet' href='<c:url value = "/styles/blocks.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/bookBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/links.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/filterBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/classes.css"/>'>

<fmt:setBundle basename="resources" var="label"/>
    <div id="header">
        <a href="<c:url value = "/app/index"/>" class="title"><fmt:message key="title" bundle="${label}"/></a>
    </div>
<div id="userId">
    <span class="boldTitle>"> <mytag:writeName/></span>
</div>
    <div id="headerLine" class="dark">
        <div id="locale">
            <a href="/app/locale?value=en"><img align='left' class="langImg" style='cursor: pointer;'  title='English'
                                                src='<c:url value = "/images/locale/en.png"/>'/> </a>
            <a href="/app/locale?value=ru"><img align='left' class="langImg" style='cursor: pointer;'  title='Русский'
                                                src='<c:url value = "/images/locale/ru.png"/>'/></a>
        </div>
        <div id="navigation">
            <div>
                <c:if test="${sessionScope.userData eq null}">
                    <a href="<c:url value="/app/registration"/>" class="light"><fmt:message key="regButton"
                                                                                            bundle="${label}"/> </a>
                    <a href="<c:url value="/app/authorization"/>" class="light"><fmt:message key="signIn"
                                                                                             bundle="${label}"/> </a>
                </c:if>

                <c:if test="${sessionScope.userData ne null}">
                    <a href="<c:url value="/app/authorization?log=out"/>" class="light"><fmt:message key="signOut"
                                                                                                     bundle="${label}"/> </a>

                    <c:if test="${sessionScope.userData.isAdmin() eq false}">
                        <a href="<c:url value="/app/personal/cabinet"/>" class="light"><fmt:message key="cabinetButton"
                                                                                                    bundle="${label}"/> </a>
                    </c:if>

                    <c:if test="${sessionScope.userData.isAdmin() eq true}">
                        <a href="<c:url value="/app/admin/panel"/>" class="light"><fmt:message key="manageButton"
                                                                                               bundle="${label}"/> </a>
                    </c:if>
                </c:if>
            </div>
        </div>
    </div>

</html>
