<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<link rel='stylesheet' href='<c:url value = "/styles/blocks.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/bookBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/links.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/filterBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/classes.css"/>'>
<script type="text/javascript" src='<c:url value = "/javascript/jquery-3.1.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/functions.js"/>'></script>
<fmt:setBundle basename="resources" var="label"/>
<body background=<c:url value="/images/bg.png"/>>
<div id="container">
    <jsp:include page="header.jsp"></jsp:include>

    <div id="authBox">

        <form action="/app/registration" method="POST" accept-charset="UTF-8">
            <c:if test="${requestScope.regErr ne null}">
                <p class="error"><fmt:message key="regError" bundle="${label}"/></p>
            </c:if>

                <table class="reg">
                    <tr>
                        <td>
                            <p class="label"><fmt:message key="loginLabel" bundle="${label}"/></p>
                        </td>
                        <td>
                            <input name="login" class="login" type="text" size="45" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="label"><fmt:message key="passwLabel" bundle="${label}"/></p>
                        </td>
                        <td>
                            <input name="password" class="login" type="password" size="45" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="label"><fmt:message key="hintFirstName" bundle="${label}"/></p>
                        </td>
                        <td>
                            <input name="firstName" class="login" type="text" size="45" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="label"><fmt:message key="hintLastName" bundle="${label}"/></p>
                        </td>
                        <td>
                            <input name="lastName" class="login" type="text" size="45" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="label"><fmt:message key="cabBirth" bundle="${label}"/></p>
                        </td>
                        <td>
                            <input name="birthDate" class="login" type="date" size="45" required/>
                        </td>
                    </tr>
                </table>


            <p><input class="buttonInput" type="submit" value="<fmt:message key="buttonConfirm" bundle="${label}"/>">
            </p>
        </form>
    </div>

</div>

</body>
</html>
