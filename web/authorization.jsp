<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<link rel='stylesheet' href='<c:url value = "/styles/blocks.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/bookBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/links.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/filterBlock.css"/>'>
<link rel='stylesheet' href='<c:url value = "/styles/classes.css"/>'>
<script type="text/javascript" src='<c:url value = "/javascript/jquery-3.1.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value = "/javascript/functions.js"/>'></script>
<fmt:setBundle basename="resources" var="label"/>
<body background=<c:url value="/images/bg.png"/>>
<div id="container">
    <jsp:include page="header.jsp"></jsp:include>

    <div id="authBox">

        <form action="/app/authorization" method="POST">
            <c:if test="${requestScope.authErr ne null}">
                <p class="error"><fmt:message key="authError" bundle="${label}"/></p>
            </c:if>
            <p class="label"><fmt:message key="loginLabel" bundle="${label}"/></p>
            <input name="login" class="login" type="text" size="45" required/>
            <p class="label"><fmt:message key="passwLabel" bundle="${label}"/></p>
            <input name="password" class="login" type="password" size="45" required/>
            <p><input class="buttonInput" type="submit" value="<fmt:message key="buttonConfirm" bundle="${label}"/>">
            </p>
        </form>
    </div>

</div>

</body>
</html>
